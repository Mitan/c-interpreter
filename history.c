#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define INPUT_MAX_LENGTH 200

typedef struct ListElement {
    char* data;
    struct ListElement * next;

} ListElement_type;

void showHistory(ListElement_type *headOfHistoryList) {
    if(headOfHistoryList == NULL) {
        puts("List is empty");
        return;
    }

    ListElement_type *current = headOfHistoryList;
    int i = 1;

    do {
        printf("%d. %s\n", i++, current->data);
        current = current->next;
    } while (current != NULL);
}

int historySize(ListElement_type *headOfHistoryList)
{
    int counter = 0;
    if (headOfHistoryList == NULL) return counter;
    else {
        ListElement_type *current = headOfHistoryList;
            do {
            counter++;
            current = current->next;
            } while (current != NULL);
    }
    return counter;
}
	
void addHistoryElement(ListElement_type **headOfHistoryList, char* text)
{	
    printf("History: %pn\n", &headOfHistoryList);
	if(headOfHistoryList == NULL) {
		(*headOfHistoryList) = (ListElement_type *) malloc(sizeof(ListElement_type));
        (*headOfHistoryList)->data = (char*) malloc(sizeof(char) * INPUT_MAX_LENGTH);
   		strcpy((*headOfHistoryList)->data, text);
    	(*headOfHistoryList)->next = NULL;
	} else {
        if (historySize(*headOfHistoryList) >= 20) {
            ListElement_type * newListHead = NULL;
 
            newListHead = (*headOfHistoryList)->next;
            free(*headOfHistoryList);
            *headOfHistoryList = newListHead;	
        }
		ListElement_type *current = *headOfHistoryList;
	
	    while (current->next != NULL) {
	        current = current->next;
	    }

	    current->next = (ListElement_type * )malloc(sizeof(ListElement_type));
        current->next->data = (char*) malloc(INPUT_MAX_LENGTH);
	    strcpy(current->next->data, text);
	    current->next->next = NULL;	
	}
}

// int main(int argc, const char* argv[]) {
//     showHistory(headOfHistoryList);
//     return 0;
// }