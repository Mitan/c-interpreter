#include <stdio.h>
#include <stdlib.h>
#include "history.h"

extern ListElement_type *headOfHistoryList;

int main(int argc, const char* argv[]) {
    printf("%pn\n", &headOfHistoryList);
    showHistory(headOfHistoryList);
    return 0;
}